import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, world!")
            
            BPM()
            
            Button {
                WorkoutManager.shared.start()
            } label: {
                Text("start sesh")
            }
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
