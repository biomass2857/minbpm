import SwiftUI
import HealthKit

class AppDelegate: NSObject, UIApplicationDelegate {
    func applicationShouldRequestHealthAuthorization(_ application: UIApplication) {
        Task {
            try await HKHealthStore().handleAuthorizationForExtension()
        }
    }
}

@main
struct MinBPMApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
