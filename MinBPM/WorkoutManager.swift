import HealthKit
import Foundation
import Combine

final class WorkoutManager: NSObject, HKWorkoutSessionDelegate {
    private let healthStore = HKHealthStore()
    static let shared = WorkoutManager()
    
    private var session: HKWorkoutSession?
    
    struct HeartRateObject: Decodable {
        let heartRate: Int
    }
    
    private let heartRateSubject = CurrentValueSubject<Int?, Never>(nil)
    var heartRatePublisher: AnyPublisher<Int?, Never> {
        heartRateSubject.eraseToAnyPublisher()
    }
    
    func start() {
        let configuration = HKWorkoutConfiguration()
        configuration.activityType = .traditionalStrengthTraining
        configuration.locationType = .indoor
        
        healthStore.workoutSessionMirroringStartHandler = { [weak self] session in
            self?.session = session
            session.delegate = self
        }
        
        Task {
            do {
                try await healthStore.startWatchApp(toHandle: configuration)
            } catch {
                debugPrint("error = " + String(describing: error))
            }
        }
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        debugPrint("didchange to " + String(describing: toState))
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        debugPrint(String(describing: error))
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didReceiveDataFromRemoteWorkoutSession data: [Data]) {
        guard let blob = data.last else { return }
        let hrObj = try! JSONDecoder().decode(HeartRateObject.self, from: blob)
        
        heartRateSubject.send(hrObj.heartRate)
    }
}
