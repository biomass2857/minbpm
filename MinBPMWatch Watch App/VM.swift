import SwiftUI
import Foundation
import Combine

final class VM: ObservableObject {
    @Published var workoutRunning = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(workoutRunning: Bool = false) {
        WorkoutManager.shared.workoutRunning
            .receive(on: DispatchQueue.main)
            .sink { [weak self] running in
                self?.workoutRunning = running
            }
            .store(in: &subscriptions)
    }
}
