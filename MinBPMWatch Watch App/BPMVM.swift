import SwiftUI
import Combine

class BPMVM: ObservableObject {
    @Published var heartRate: Int = 50
    private var cancelBag = Set<AnyCancellable>()
    
    init() {
        WorkoutManager.shared.heartRatePublisher
            .sink {
                self.heartRate = $0 ?? -1
            }
            .store(in: &cancelBag)
    }
}
