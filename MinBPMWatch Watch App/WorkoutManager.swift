import HealthKit
import Combine
import Foundation

final class WorkoutManager: NSObject, HKLiveWorkoutBuilderDelegate, HKWorkoutSessionDelegate {
    private let healthStore = HKHealthStore()
    static let shared = WorkoutManager()
    
    private let heartRate = HKQuantityType(.heartRate)
    private let workoutType = HKObjectType.workoutType()
    
    private let encoder = JSONEncoder()
    
    struct HeartRateObject: Encodable {
        let heartRate: Int
    }
    
    private var session: HKWorkoutSession?
    private var builder: HKWorkoutBuilder?
    
    private var workoutRunningSubject = CurrentValueSubject<Bool, Never>(false)
    
    private let heartRateSubject = CurrentValueSubject<Int?, Never>(nil)
    
    var heartRatePublisher: AnyPublisher<Int?, Never> {
        heartRateSubject.eraseToAnyPublisher()
    }
    
    func tryStartWorkout(configuration: HKWorkoutConfiguration) {
        healthStore.requestAuthorization(toShare: [workoutType], read: [heartRate]) { success, error in
            if success {
                Task {
                    await self.startWorkout(configuration: configuration)
                }
            } else {
                debugPrint(String(describing: error))
            }
        }
    }
    
    private func startWorkout(configuration: HKWorkoutConfiguration) async {
        let session: HKWorkoutSession
        do {
            session = try HKWorkoutSession(healthStore: healthStore, configuration: configuration)
        } catch {
            fatalError("cannot start HKWorkoutSession")
        }
        
        let builder = session.associatedWorkoutBuilder()
        let source = HKLiveWorkoutDataSource(healthStore: healthStore, workoutConfiguration: configuration)
        source.enableCollection(for: heartRate, predicate: nil)
        builder.dataSource = source
        
        session.delegate = self
        builder.delegate = self
        
        self.session = session
        self.builder = builder
        
        let start = Date.now
        
        do {
            try await builder.beginCollection(at: start)
        } catch {
            fatalError("couldnt start collecting " + String(describing: error))
        }
        
        do {
            try await session.startMirroringToCompanionDevice()
        } catch {
            fatalError("couldnt mirror session " + String(describing: error))
        }
        
        session.startActivity(with: start)
        
        debugPrint("session started")
        
        workoutRunningSubject.send(true)
    }
    
    func stop() {
        session?.stopMirroringToCompanionDevice { [weak self] success, error in
            self?.session?.stopActivity(with: .now)
            self?.workoutRunningSubject.send(false)
        }
    }
    
    var workoutRunning: AnyPublisher<Bool, Never> {
        workoutRunningSubject.eraseToAnyPublisher()
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        debugPrint("new state: " + String(describing: toState))
        
        if toState == .ended {
            builder?.endCollection(withEnd: Date()) { [weak self] success, error in
                self?.builder?.finishWorkout { _, _ in
                    
                }
            }
        }
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        debugPrint(String(describing: error))
    }
    
    func workoutBuilder(_ workoutBuilder: HKLiveWorkoutBuilder, didCollectDataOf collectedTypes: Set<HKSampleType>) {
        let predicate = HKQuery.predicateForSamples(withStart: Date.distantPast, end: Date(), options: .strictEndDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        let query = HKSampleQuery(
            sampleType: heartRate,
            predicate: predicate,
            limit: 1,
            sortDescriptors: [sortDescriptor]
        ) { [weak self] _, results, error in
            guard let self else { return }
            guard error == nil else {
                debugPrint("Error: \(error!.localizedDescription)")
                return
            }
            
            guard let heartRateSample = results?.first as? HKQuantitySample else {
                return
            }
            
            let unit: HKUnit = .count().unitDivided(by: .minute())
            let heartRateNumber = Int(heartRateSample.quantity.doubleValue(for: unit))
            
            let hrObj = HeartRateObject(heartRate: heartRateNumber)
            do {
                let data = try self.encoder.encode(hrObj)
                self.session?.sendToRemoteWorkoutSession(data: data) { _, _ in }
            } catch {
                debugPrint("error while encoding: " + String(describing: error))
            }
            
            debugPrint("fetched heartRateNumber \(heartRateNumber)")
            
            self.heartRateSubject.send(heartRateNumber)
        }
        
        HKHealthStore().execute(query)
    }
    
    func workoutBuilderDidCollectEvent(_ workoutBuilder: HKLiveWorkoutBuilder) {
        debugPrint("didCollectEvent")
    }
}
