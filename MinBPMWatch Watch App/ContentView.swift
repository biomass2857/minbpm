import SwiftUI
import Combine

struct ContentView: View {
    @ObservedObject var viewModel = VM()
    
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, world!")
            
            if viewModel.workoutRunning {
                BPM()
                Text("workout running")
            } else {
                Text("no workout running")
            }
            
            Button {
                WorkoutManager.shared.stop()
            } label: {
                Text("stop")
            }
        }
        .padding()
    }
}
