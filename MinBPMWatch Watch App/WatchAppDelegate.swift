import SwiftUI
import HealthKit
import Foundation

final class WatchAppDelegate: NSObject, WKApplicationDelegate {
    func handle(_ workoutConfiguration: HKWorkoutConfiguration) {
        WorkoutManager.shared.tryStartWorkout(configuration: workoutConfiguration)
    }
}
