import SwiftUI

struct AnimationValues {
    var scale: CGFloat = 1.0
}

struct BPM: View {
    @StateObject var vm = BPMVM()
    
    var body: some View {
        HStack {
            Image(systemName: "heart.fill")
                .resizable()
                .renderingMode(.template)
                .scaledToFit()
                .frame(width: 16, height: 16)
                .foregroundColor(.red)
                .scaleEffect(2)
                .keyframeAnimator(
                    initialValue: AnimationValues(),
                    repeating: true,
                    content: { view, values  in
                        view
                            .scaleEffect(values.scale)
                    },
                    keyframes: { _ in
                        KeyframeTrack(\.scale) {
                            LinearKeyframe(0.8, duration: 60 / Double(vm.heartRate))
                        }
                    }
                )
                .id(vm.heartRate)
            Spacer()
                .frame(width: 16)
            Text("\(vm.heartRate)")
                .monospaced()
        }
    }
}
